package Clases;

public class Empleado {
    String nombre, cargo;
    Double nominal, desembolo_sem, devengado, eficiencia,h_semanales;
    int anio;
    Double septimo, aguinaldo, vacaciones, isss, afp, insaforp, factor_recargo;

    public Empleado() {
    }

    public Double getFactor_recargo() {
        return factor_recargo;
    }

    public void setFactor_recargo(Double factor_recargo) {
        this.factor_recargo = factor_recargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCargo() {
        return cargo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getNominal() {
        return nominal;
    }

    public void setNominal(Double nominal) {
        this.nominal = nominal;
    }

    public Double getDesembolo_sem() {
        return desembolo_sem;
    }

    public void setDesembolo_sem(Double desembolo_sem) {
        this.desembolo_sem = desembolo_sem;
    }

    public Double getDevengado() {
        return devengado;
    }

    public void setDevengado(Double devengado) {
        this.devengado = devengado;
    }

    public Double getEficiencia() {
        return eficiencia;
    }

    public void setEficiencia(Double eficiencia) {
        this.eficiencia = eficiencia;
    }

    public Double getH_semanales() {
        return h_semanales;
    }

    public void setH_semanales(Double h_semanales) {
        this.h_semanales = h_semanales;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public Double getSeptimo() {
        return septimo;
    }

    public void setSeptimo(Double septimo) {
        this.septimo = septimo;
    }

    public Double getAguinaldo() {
        return aguinaldo;
    }

    public void setAguinaldo(Double aguinaldo) {
        this.aguinaldo = aguinaldo;
    }

    public Double getVacaciones() {
        return vacaciones;
    }

    public void setVacaciones(Double vacaciones) {
        this.vacaciones = vacaciones;
    }

    public Double getIsss() {
        return isss;
    }

    public void setIsss(Double isss) {
        this.isss = isss;
    }

    public Double getAfp() {
        return afp;
    }

    public void setAfp(Double afp) {
        this.afp = afp;
    }

    public Double getInsaforp() {
        return insaforp;
    }

    public void setInsaforp(Double insaforp) {
        this.insaforp = insaforp;
    }
    
    
    
}
