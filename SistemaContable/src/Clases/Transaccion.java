package Clases;

public class Transaccion {

    private int idCuenta;
    private double debe, haber, ivaAcreditable, ivaRetenido;
    private String nombreCuenta;

    public String getNombreCuenta() {
        return nombreCuenta;
    }

    public void setNombreCuenta(String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }

    public Transaccion() {
    }

    public int getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(int idCuenta) {
        this.idCuenta = idCuenta;
    }

    public double getDebe() {
        return debe;
    }

    public void setDebe(double debe) {
        this.debe = debe;
    }

    public double getHaber() {
        return haber;
    }

    public void setHaber(double haber) {
        this.haber = haber;
    }

    public double getIvacredito() {
        return ivaAcreditable;
    }

    public void setIvacredito(double ivacredito) {
        this.ivaAcreditable = ivacredito;
    }

    public double getIvadebito() {
        return ivaRetenido;
    }

    public void setIvadebito(double ivadebito) {
        this.ivaRetenido = ivadebito;
    }

}
