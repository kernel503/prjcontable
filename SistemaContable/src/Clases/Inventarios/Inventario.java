/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.Inventarios;

import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class Inventario {
    private int idInventario;
    private String nombreInventario;
    private String descInventario;
    private String metodoValuacion;
    private int totalUnidades;
    private float totalPrecioUnidades;
    
    private ArrayList<Articulo> articulos;
    private ArrayList<Kardex> kardexs;
    
    public Inventario(){
    }
    
    public Inventario(int idInventario, String nombreInventario, String descInventario, String metodoValuacion, int totalUnidades, float totalPrecioUnidades, ArrayList<Articulo> articulos, ArrayList<Kardex> kardexs) {
        this.idInventario = idInventario;
        this.nombreInventario = nombreInventario;
        this.descInventario = descInventario;
        this.metodoValuacion = metodoValuacion;
        this.totalUnidades = totalUnidades;
        this.totalPrecioUnidades = totalPrecioUnidades;
        this.articulos = articulos;
        this.kardexs = kardexs;
    }
    
    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public String getNombreInventario() {
        return nombreInventario;
    }

    public void setNombreInventario(String nombreInventario) {
        this.nombreInventario = nombreInventario;
    }

    public String getDescInventario() {
        return descInventario;
    }

    public void setDescInventario(String descInventario) {
        this.descInventario = descInventario;
    }

    public String getMetodoValuacion() {
        return metodoValuacion;
    }

    public void setMetodoValuacion(String metodoValuacion) {
        this.metodoValuacion = metodoValuacion;
    }

    public int getTotalUnidades() {
        return totalUnidades;
    }

    public void setTotalUnidades(int totalUnidades) {
        this.totalUnidades = totalUnidades;
    }

    public float getTotalPrecioUnidades() {
        return totalPrecioUnidades;
    }

    public void setTotalPrecioUnidades(float totalPrecioUnidades) {
        this.totalPrecioUnidades = totalPrecioUnidades;
    }

    public ArrayList<Articulo> getArticulo() {
        return articulos;
    }

    public void setArticulo(ArrayList<Articulo> articulos) {
        this.articulos = articulos;
    }

    public ArrayList<Kardex> getKardexs() {
        return kardexs;
    }

    public void setKardexs(ArrayList<Kardex> kardexs) {
        this.kardexs = kardexs;
    }
    
    public String toString(){
        return "Codigo:\n" + this.idInventario + "\n" +
                "Nombre:\n" + this.nombreInventario + "\n" +
                "Descripcion:\n"+ this.descInventario +"\n" +
                "Metodo de Valuacion:\n" + this.metodoValuacion + "\n" +
                "Total Unidades(cantidad):\n" + this.totalUnidades + "\n" +
                "Total Unidades($)\n" + this.totalPrecioUnidades + "\n";        
    }
        
}
