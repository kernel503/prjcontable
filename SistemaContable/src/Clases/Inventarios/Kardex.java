/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.Inventarios;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Daniel
 */
public class Kardex {
    private int idKardex;
    private int cantMaxima;
    private int cantMinima;
    private int totalEntradas;
    private int totalSalidas;
    private int existenciaTotal;
    private float totalPrecioEntradas;
    private float totalPrecioSalidas;
    private float precioExistenciaTotal;
    private Date fechaInicio;
    private Date fechaFin;
    private String elaboro;
    private String reviso;
    private String autorizo;
    private String paraProducir;
    
    private Articulo articulo;    
    private ArrayList<RegistroIO> registrosio;
    
    public Kardex(){
    }

    public Kardex(int idKardex, int cantMaxima, int cantMinima, int totalEntradas, int totalSalidas, int existenciaTotal, float totalPrecioEntradas, float totalPrecioSalidas, float precioExistenciaTotal, Date fechaInicio, Date fechaFin, String elaboro, String reviso, String autorizo, String paraProducir, Articulo articulo, ArrayList<RegistroIO> registrosio) {
        this.idKardex = idKardex;
        this.cantMaxima = cantMaxima;
        this.cantMinima = cantMinima;
        this.totalEntradas = totalEntradas;
        this.totalSalidas = totalSalidas;
        this.existenciaTotal = existenciaTotal;
        this.totalPrecioEntradas = totalPrecioEntradas;
        this.totalPrecioSalidas = totalPrecioSalidas;
        this.precioExistenciaTotal = precioExistenciaTotal;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.elaboro = elaboro;
        this.reviso = reviso;
        this.autorizo = autorizo;
        this.paraProducir = paraProducir;
        this.articulo = articulo;
        this.registrosio = registrosio;
    }

    public int getIdKardex() {
        return idKardex;
    }

    public void setIdKardex(int idKardex) {
        this.idKardex = idKardex;
    }

    public int getCantMaxima() {
        return cantMaxima;
    }

    public void setCantMaxima(int cantMaxima) {
        this.cantMaxima = cantMaxima;
    }

    public int getCantMinima() {
        return cantMinima;
    }

    public void setCantMinima(int cantMinima) {
        this.cantMinima = cantMinima;
    }

    public int getTotalEntradas() {
        return totalEntradas;
    }

    public void setTotalEntradas(int totalEntradas) {
        this.totalEntradas = totalEntradas;
    }

    public int getTotalSalidas() {
        return totalSalidas;
    }

    public void setTotalSalidas(int totalSalidas) {
        this.totalSalidas = totalSalidas;
    }

    public int getExistenciaTotal() {
        return existenciaTotal;
    }

    public void setExistenciaTotal(int existenciaTotal) {
        this.existenciaTotal = existenciaTotal;
    }

    public float getTotalPrecioEntradas() {
        return totalPrecioEntradas;
    }

    public void setTotalPrecioEntradas(float totalPrecioEntradas) {
        this.totalPrecioEntradas = totalPrecioEntradas;
    }

    public float getTotalPrecioSalidas() {
        return totalPrecioSalidas;
    }

    public void setTotalPrecioSalidas(float totalPrecioSalidas) {
        this.totalPrecioSalidas = totalPrecioSalidas;
    }

    public float getPrecioExistenciaTotal() {
        return precioExistenciaTotal;
    }

    public void setPrecioExistenciaTotal(float precioExistenciaTotal) {
        this.precioExistenciaTotal = precioExistenciaTotal;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getElaboro() {
        return elaboro;
    }

    public void setElaboro(String elaboro) {
        this.elaboro = elaboro;
    }

    public String getReviso() {
        return reviso;
    }

    public void setReviso(String reviso) {
        this.reviso = reviso;
    }

    public String getAutorizo() {
        return autorizo;
    }

    public void setAutorizo(String autorizo) {
        this.autorizo = autorizo;
    }

    public String getParaProducir() {
        return paraProducir;
    }

    public void setParaProducir(String paraProducir) {
        this.paraProducir = paraProducir;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public ArrayList<RegistroIO> getRegistrosio() {
        return registrosio;
    }

    public void setRegistrosio(ArrayList<RegistroIO> registrosio) {
        this.registrosio = registrosio;
    }

}
