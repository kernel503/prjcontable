/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.Inventarios;

import java.util.Date;

/**
 *
 * @author Daniel
 */
public class RegistroIO {
    private int idRegistro;
    private String tipoMovimiento;
    private String tipoRegistro;
    private Date fechaRealizacion;    
    private String descRegistroIO;
    private int cantidadIO;
    private float precioUnitarioIO;
    private float totalPrecioIO;
    private int cantidadExistencias;
    private float precioExistencias;
    private float totalPrecioExistencias;
        
    public RegistroIO(){
    }

    public RegistroIO(int idRegistro, String tipoMovimiento, String tipoRegistro, Date fechaRealizacion, String descRegistroIO, int cantidadIO, float precioUnitarioIO, float totalPrecioIO, int cantidadExistencias, float precioExistencias, float totalPrecioExistencias) {
        this.idRegistro = idRegistro;
        this.tipoMovimiento = tipoMovimiento;
        this.tipoRegistro = tipoRegistro;
        this.fechaRealizacion = fechaRealizacion;        
        this.descRegistroIO = descRegistroIO;
        this.cantidadIO = cantidadIO;
        this.precioUnitarioIO = precioUnitarioIO;
        this.totalPrecioIO = totalPrecioIO;
        this.cantidadExistencias = cantidadExistencias;
        this.precioExistencias = precioExistencias;
        this.totalPrecioExistencias = totalPrecioExistencias;
    }

    public int getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(int idRegistro) {
        this.idRegistro = idRegistro;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public Date getFechaRealizacion() {
        return fechaRealizacion;
    }

    public void setFechaRealizacion(Date fechaRealizacion) {
        this.fechaRealizacion = fechaRealizacion;
    }

    public String getDescRegistroIO() {
        return descRegistroIO;
    }

    public void setDescRegistroIO(String descRegistroIO) {
        this.descRegistroIO = descRegistroIO;
    }

    public int getCantidadIO() {
        return cantidadIO;
    }

    public void setCantidadIO(int cantidadIO) {
        this.cantidadIO = cantidadIO;
    }

    public float getPrecioUnitarioIO() {
        return precioUnitarioIO;
    }

    public void setPrecioUnitarioIO(float precioUnitarioIO) {
        this.precioUnitarioIO = precioUnitarioIO;
    }

    public float getTotalPrecioIO() {
        return totalPrecioIO;
    }

    public void setTotalPrecioIO(float totalPrecioIO) {
        this.totalPrecioIO = totalPrecioIO;
    }

    public int getCantidadExistencias() {
        return cantidadExistencias;
    }

    public void setCantidadExistencias(int cantidadExistencias) {
        this.cantidadExistencias = cantidadExistencias;
    }

    public float getPrecioExistencias() {
        return precioExistencias;
    }

    public void setPrecioExistencias(float precioExistencias) {
        this.precioExistencias = precioExistencias;
    }

    public float getTotalPrecioExistencias() {
        return totalPrecioExistencias;
    }

    public void setTotalPrecioExistencias(float totalPrecioExistencias) {
        this.totalPrecioExistencias = totalPrecioExistencias;
    }
            
}
