/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.Inventarios;

import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class Articulo {
    private String codigoArticulo;
    private String tipoArticulo;
    private String nombreArticulo;
    private String descArticulo;
    private String ubicacion;
    private String proveedor;
    private String unidadMedida;
    
    private ArrayList<Articulo> materiales;
    
    public Articulo(){
    }

    public Articulo(String codigoArticulo, String tipoArticulo, String nombreArticulo, String descArticulo, String ubicacion, String proveedor, String unidadMedida, ArrayList<Articulo> materiales) {
        this.codigoArticulo = codigoArticulo;
        this.tipoArticulo = tipoArticulo;
        this.nombreArticulo = nombreArticulo;
        this.descArticulo = descArticulo;
        this.ubicacion = ubicacion;
        this.proveedor = proveedor;
        this.unidadMedida = unidadMedida;
        this.materiales = materiales;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public String getDescArticulo() {
        return descArticulo;
    }

    public void setDescArticulo(String descArticulo) {
        this.descArticulo = descArticulo;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }
    
    public ArrayList<Articulo> getMateriales() {
        return materiales;
    }

    public void setMateriales(ArrayList<Articulo> materiales) {
        this.materiales = materiales;
    }
    
    
}
