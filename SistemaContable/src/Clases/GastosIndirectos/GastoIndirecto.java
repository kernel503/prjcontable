/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases.GastosIndirectos;

import java.util.Date;

/**
 *
 * @author Daniel
 */
public class GastoIndirecto {
    private int idGIF;
    private String concepto;
    private Date fecha;
    private String descGastoif;
    private float valor;
    
    public GastoIndirecto(){
    }

    public GastoIndirecto(int idGIF, String concepto, Date fecha, String descGastoif, float valor) {
        this.idGIF = idGIF;
        this.concepto = concepto;
        this.fecha = fecha;
        this.descGastoif = descGastoif;
        this.valor = valor;
    }

    public int getIdGIF() {
        return idGIF;
    }

    public void setIdGIF(int idGIF) {
        this.idGIF = idGIF;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescGastoif() {
        return descGastoif;
    }

    public void setDescGastoif(String descGastoif) {
        this.descGastoif = descGastoif;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
    
}
