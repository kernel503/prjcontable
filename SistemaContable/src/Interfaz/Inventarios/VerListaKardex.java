/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz.Inventarios;

import BDConexion.Conexion;
import Clases.Inventarios.Articulo;
import Clases.Inventarios.Inventario;
import Clases.Inventarios.Kardex;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class VerListaKardex extends javax.swing.JInternalFrame {
    private Articulo articulo;
    private Inventario inventario;
    private Kardex kardexNuevo;
    private Conexion cn;
    private DefaultTableModel tblmdListaKardex;

    /**
     * Creates new form VerListaDeKardex
     */
    public VerListaKardex(Articulo articulo, Inventario inventario) {
        this.articulo = articulo;
        this.inventario = inventario;
        initComponents();
        llenarTablaListaKardex();
        llenarListaProduccion();
        llenarTotalesListaKardex();
        this.setTitle("Registro de kardex - Artículo " + articulo.getNombreArticulo() + "(" + articulo.getCodigoArticulo() + ")");
        this.repaint();
    }
    
    public void llenarListaProduccion(){
        cn = new Conexion();
        this.cmbProduccion.addItem("N/A");
        if(this.inventario.getIdInventario() != 1){
            for(String articulo: cn.obtenerNombresProductos()){
                this.cmbProduccion.addItem(articulo);
            }                
        }
        
    }
    
    public void llenarTablaListaKardex(){
        this.tblmdListaKardex = (DefaultTableModel)this.tblListaKardex.getModel();
        cn = new Conexion();
        
        for(Kardex kard: cn.obtenerKardexPorArticulo(this.articulo.getCodigoArticulo())){
            Object data[] = {
                kard.getIdKardex(),
                kard.getFechaInicio(),
                kard.getFechaFin(),
                kard.getTotalEntradas(),
                kard.getTotalSalidas(),
                kard.getExistenciaTotal(),
                kard.getTotalPrecioEntradas(),
                kard.getTotalPrecioSalidas(),
                kard.getPrecioExistenciaTotal()
            };
            
            this.tblmdListaKardex.addRow(data);
        }
        this.tblListaKardex.setModel(this.tblmdListaKardex);
    }
    
    public void actualizarTablaListaKardex(){
        Object data[] = {
            this.kardexNuevo.getIdKardex(),
            this.kardexNuevo.getFechaInicio(),
            this.kardexNuevo.getFechaFin(),
            this.kardexNuevo.getTotalEntradas(),
            this.kardexNuevo.getTotalSalidas(),
            this.kardexNuevo.getExistenciaTotal(),
            this.kardexNuevo.getTotalPrecioEntradas(),
            this.kardexNuevo.getTotalPrecioSalidas(),
            this.kardexNuevo.getPrecioExistenciaTotal()
        };
        this.tblmdListaKardex.addRow(data);  
        this.tblListaKardex.repaint();        
    }
    
    public void llenarTablaListaKardexPorParametros(int mes, int year, String produccion, String codigoArticulo){
        this.tblmdListaKardex = (DefaultTableModel)this.tblListaKardex.getModel();
        if(this.tblmdListaKardex.getRowCount() > 0){ this.tblmdListaKardex.setRowCount(0); }
        cn = new Conexion();
        
        for(Kardex kard: cn.obtenerKardexPorFechaInicioProduccion(mes, year, produccion, codigoArticulo)){
            Object data[] = {
                kard.getIdKardex(),
                kard.getFechaInicio(),
                kard.getFechaFin(),
                kard.getTotalEntradas(),
                kard.getTotalSalidas(),
                kard.getExistenciaTotal(),
                kard.getTotalPrecioEntradas(),
                kard.getTotalPrecioSalidas(),
                kard.getPrecioExistenciaTotal()
            };
            
            this.tblmdListaKardex.addRow(data);
        }
        this.tblListaKardex.setModel(this.tblmdListaKardex);
        this.tblListaKardex.repaint();
    }
    
    public void llenarTotalesListaKardex(){
        
        int i = 0, sumaEntradas = 0, sumaSalidas = 0, sumaExistencias = 0;
        float sumaEntradasPrecio = 0, sumaSalidasPrecio = 0, sumaExistenciasPrecio = 0;
        
        while(i < this.tblmdListaKardex.getRowCount()){
            sumaEntradas = sumaEntradas + (int)this.tblmdListaKardex.getValueAt(i, 3);
            sumaSalidas = sumaSalidas + (int)this.tblmdListaKardex.getValueAt(i, 4);
            sumaExistencias = sumaExistencias + (int)this.tblmdListaKardex.getValueAt(i, 5);
            sumaEntradasPrecio = sumaEntradasPrecio + (float)this.tblmdListaKardex.getValueAt(i, 6);
            sumaSalidasPrecio = sumaSalidasPrecio + (float)this.tblmdListaKardex.getValueAt(i, 7);
            sumaExistenciasPrecio = sumaExistenciasPrecio + (float)this.tblmdListaKardex.getValueAt(i, 8);
            i++;
        }        
        
        this.txtTotalEntradas.setText(String.valueOf(sumaEntradas));
        this.txtTotalPrecioEntradas.setText(String.valueOf(sumaEntradasPrecio));        
        this.txtTotalSalidas.setText(String.valueOf(sumaSalidas));
        this.txtTotalPrecioSalidas.setText(String.valueOf(sumaSalidasPrecio));        
        this.txtTotalExistencias.setText(String.valueOf(sumaExistencias));
        this.txtTotalPrecioExistencias.setText(String.valueOf(sumaExistenciasPrecio));
        
        switch(this.inventario.getIdInventario()){
            case 1:
                this.labCosto.setText("(Costo de Ventas Total)");                
                this.labUnitarioAprox.setText("CVT Unitario(Aprox)$:");
                this.txtUnitarioAprox.setText(String.valueOf(sumaSalidasPrecio / sumaSalidas));
                this.labUnitarioAprox.setVisible(true);
                this.txtUnitarioAprox.setVisible(true);
                break;
            case 2:
                this.labCosto.setText("(Total Material Usado)");
                this.labUnitarioAprox.setVisible(false);
                this.txtUnitarioAprox.setVisible(false);
                break;
            case 3:
                this.labCosto.setText("");
                this.labUnitarioAprox.setVisible(false);
                this.txtUnitarioAprox.setVisible(false);
                break;
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblListaKardex = new javax.swing.JTable();
        btnCrearKardex = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        cmbProduccion = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        spYear = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        cmbMes = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtTotalEntradas = new javax.swing.JTextField();
        txtTotalPrecioEntradas = new javax.swing.JTextField();
        txtTotalSalidas = new javax.swing.JTextField();
        txtTotalPrecioSalidas = new javax.swing.JTextField();
        txtTotalExistencias = new javax.swing.JTextField();
        txtTotalPrecioExistencias = new javax.swing.JTextField();
        labUnitarioAprox = new javax.swing.JLabel();
        txtUnitarioAprox = new javax.swing.JTextField();
        labCosto = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        tblListaKardex.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Fecha Inicio", "Fecha Fin", "Total de Entradas", "Total de Salidas", "Total Existencias", "Total de Entradas($)", "Total de Salidas($)", "TotalExistencias($)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblListaKardex.getTableHeader().setReorderingAllowed(false);
        tblListaKardex.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblListaKardexMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblListaKardex);

        btnCrearKardex.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnCrearKardex.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add.png"))); // NOI18N
        btnCrearKardex.setText("Crear Kardex");
        btnCrearKardex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearKardexActionPerformed(evt);
            }
        });

        jLabel1.setText("(Doble click en la fila del kardex que quiere visualizar)");

        jLabel2.setText("Mes:");

        btnBuscar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/list.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        cmbProduccion.setToolTipText("seleccione para mostrar los kardexs de producción de un articulo, deje \"N/A\" para mostrar todos");

        jLabel3.setText("Para Producción de:");

        spYear.setModel(new javax.swing.SpinnerNumberModel(2019, 1900, 2099, 1));

        jLabel4.setText("Año:");

        cmbMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "N/A", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        cmbMes.setToolTipText("seleccione el mes para visualizar kardexs, dene \"N/A\" para mostrar todo el año");

        jLabel5.setText("Total Entradas(Cantidad):");

        jLabel6.setText("Total Entradas($):");

        jLabel7.setText("Total Salidas(Cantidad):");

        jLabel8.setText("Total Salidas($):");

        jLabel9.setText("Total Existencias(Cantidad):");

        jLabel10.setText("Total Existencias($):");

        labUnitarioAprox.setText("-----------------------");

        txtUnitarioAprox.setEditable(false);

        labCosto.setText("(--------)");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtTotalEntradas, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(57, 57, 57)
                                .addComponent(jLabel7))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtTotalPrecioEntradas, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(57, 57, 57)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labUnitarioAprox)
                                    .addComponent(jLabel8)
                                    .addComponent(labCosto))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtUnitarioAprox)
                            .addComponent(txtTotalSalidas)
                            .addComponent(txtTotalPrecioSalidas, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE))
                        .addGap(79, 79, 79)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTotalExistencias, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                            .addComponent(txtTotalPrecioExistencias))
                        .addGap(61, 61, 61))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cmbMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spYear, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(cmbProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCrearKardex)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbProduccion)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnCrearKardex)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(btnBuscar)
                        .addComponent(spYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(cmbMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9)
                    .addComponent(txtTotalEntradas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalSalidas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalExistencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel10)
                    .addComponent(txtTotalPrecioEntradas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalPrecioSalidas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalPrecioExistencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labCosto)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labUnitarioAprox)
                    .addComponent(txtUnitarioAprox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCrearKardexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearKardexActionPerformed
        // TODO add your handling code here:
        this.kardexNuevo = new Kardex();
        CrearKardex crearKardex = new CrearKardex(this.articulo, this.inventario, this.kardexNuevo);
        
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = crearKardex.getSize();
        crearKardex.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 2);
        this.getParent().add(crearKardex);
        crearKardex.show();        
    }//GEN-LAST:event_btnCrearKardexActionPerformed

    private void tblListaKardexMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblListaKardexMouseClicked
        // TODO add your handling code here:
        cn = new Conexion();
        int clicks = evt.getClickCount();
        int filaSeleccionada = this.tblListaKardex.rowAtPoint(evt.getPoint());
        
        if(clicks == 2){
            int idKardex = (int)this.tblmdListaKardex.getValueAt(filaSeleccionada,0);
            Kardex kardex = cn.obtenerKardexPorId(idKardex);            
            
            VerKardex verKardex = new VerKardex(this.articulo, this.inventario, kardex);
        
            Dimension desktopSize = this.getParent().getSize();
            Dimension jInternalFrameSize = verKardex.getSize();
            verKardex.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                    (desktopSize.height - jInternalFrameSize.height) / 2);
            this.getParent().add(verKardex);
            verKardex.show();
        }
    }//GEN-LAST:event_tblListaKardexMouseClicked

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        // TODO add your handling code here:
        //if(this.tblmdListaKardex.getRowCount() > 0){ this.tblmdListaKardex.setRowCount(0); }
        if(this.kardexNuevo != null){
            this.actualizarTablaListaKardex();
            this.llenarTotalesListaKardex();
            this.kardexNuevo = null;
        }        
    }//GEN-LAST:event_formInternalFrameActivated

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:        
        this.llenarTablaListaKardexPorParametros(
                this.cmbMes.getSelectedIndex() - 1,
                Integer.parseInt(this.spYear.getValue().toString()),
                String.valueOf(this.cmbProduccion.getSelectedItem()),
                this.articulo.getCodigoArticulo());
        this.llenarTotalesListaKardex();
    }//GEN-LAST:event_btnBuscarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCrearKardex;
    private javax.swing.JComboBox<String> cmbMes;
    private javax.swing.JComboBox<String> cmbProduccion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labCosto;
    private javax.swing.JLabel labUnitarioAprox;
    private javax.swing.JSpinner spYear;
    private javax.swing.JTable tblListaKardex;
    private javax.swing.JTextField txtTotalEntradas;
    private javax.swing.JTextField txtTotalExistencias;
    private javax.swing.JTextField txtTotalPrecioEntradas;
    private javax.swing.JTextField txtTotalPrecioExistencias;
    private javax.swing.JTextField txtTotalPrecioSalidas;
    private javax.swing.JTextField txtTotalSalidas;
    private javax.swing.JTextField txtUnitarioAprox;
    // End of variables declaration//GEN-END:variables
}
