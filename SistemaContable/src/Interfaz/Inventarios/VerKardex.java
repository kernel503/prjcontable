/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz.Inventarios;

import BDConexion.Conexion;
import Clases.Inventarios.Articulo;
import Clases.Inventarios.Inventario;
import Clases.Inventarios.Kardex;
import Clases.Inventarios.RegistroIO;
import java.awt.Dimension;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Daniel
 */
public class VerKardex extends javax.swing.JInternalFrame {
    private Articulo articulo;
    private Inventario inventario;
    private Kardex kardex;
    private RegistroIO registroNuevo;
    private Conexion cn;
    private DefaultTableModel tblmdRegistrosKardex;

    /**
     * Creates new form VerKardex
     */
    public VerKardex(Articulo articulo, Inventario inventario, Kardex kardex) {
        this.articulo = articulo;
        this.inventario = inventario;
        this.kardex = kardex;
        RegistroIO registroNuevo = null;//new RegistroIO();
        initComponents();
        llenarTablaRegistrosKardex();
        llenarEncabezado();
        llenarBaseTotales();
        llenarBaseFirmas();
    }
    
    public void llenarTablaRegistrosKardex(){
        cn = new Conexion();
        this.tblmdRegistrosKardex = (DefaultTableModel)this.tblRegistrosKardex.getModel();
        
        for(RegistroIO registro: cn.obtenerRegistrosPorKardex(kardex.getIdKardex())){
            if(registro.getTipoRegistro().equalsIgnoreCase("ENTRADA")){
                Object data[] = {
                    registro.getFechaRealizacion(),
                    registro.getTipoMovimiento(),
                    registro.getDescRegistroIO(),
                    registro.getCantidadIO(),
                    registro.getPrecioUnitarioIO(),
                    registro.getTotalPrecioIO(),
                    0,0.0,0.0,
                    registro.getCantidadExistencias(),
                    registro.getPrecioExistencias(),
                    registro.getTotalPrecioExistencias()
                };
                this.tblmdRegistrosKardex.addRow(data);
            }else{
                Object data[] = {
                    registro.getFechaRealizacion(),
                    registro.getTipoMovimiento(),
                    registro.getDescRegistroIO(),
                    0,0.0,0.0,
                    registro.getCantidadIO(),
                    registro.getPrecioUnitarioIO(),
                    registro.getTotalPrecioIO(),                    
                    registro.getCantidadExistencias(),
                    registro.getPrecioExistencias(),
                    registro.getTotalPrecioExistencias()
                };
                this.tblmdRegistrosKardex.addRow(data);
            }
        }
        this.tblRegistrosKardex.setModel(this.tblmdRegistrosKardex);
    }
    
    public void llenarEncabezado(){
        labArticulo.setText(articulo.getNombreArticulo());
        labCodArticulo.setText(articulo.getCodigoArticulo());
        labUbicacion.setText(articulo.getUbicacion());        
        labMaximo.setText(String.valueOf(kardex.getCantMaxima()));
        labMinimo.setText(String.valueOf(kardex.getCantMinima()));
        labProduccion.setText(kardex.getParaProducir());        
        labMetodo.setText(inventario.getMetodoValuacion());
    }
    
    public void llenarBaseTotales(){
        txtTotalEntradas.setText(String.valueOf(kardex.getTotalEntradas()));
        txtTotalPrecioEntradas.setText(String.valueOf(kardex.getTotalPrecioEntradas()));
        txtTotalSalidas.setText(String.valueOf(kardex.getTotalSalidas()));
        txtTotalPrecioSalidas.setText(String.valueOf(kardex.getTotalPrecioSalidas()));
        txtTotalExistencias.setText(String.valueOf(kardex.getExistenciaTotal()));
        txtTotalPrecioExistencias.setText(String.valueOf(kardex.getPrecioExistenciaTotal()));    
        
        switch(this.inventario.getIdInventario()){
            case 1:
                this.labCosto.setText("(Costo de Ventas)");
                this.labUnitarioAprox.setText("Costo de Ventas Unitario(Aprox)$:");
                this.txtUnitarioAprox.setText(String.valueOf(
                        kardex.getTotalPrecioSalidas() / kardex.getTotalSalidas()
                ));
                break;
            case 2:
                this.labCosto.setText("(Total Material Usado)");
                this.labUnitarioAprox.setText("");
                this.txtUnitarioAprox.setText("");
                break;
            case 3:
                this.labCosto.setText("");
                this.labUnitarioAprox.setText("");
                this.txtUnitarioAprox.setText("");
                break;                
        }
    }
    
    public void llenarBaseFirmas(){
        labElaboro.setText(kardex.getElaboro());
        labReviso.setText(kardex.getReviso());
        labAutorizo.setText(kardex.getAutorizo());
    }
    
    public void actualizarTablaRegistrosKardex(){
        if(registroNuevo.getTipoRegistro().equalsIgnoreCase("ENTRADA")){
            Object data[] = {
                registroNuevo.getFechaRealizacion(),
                registroNuevo.getTipoMovimiento(),
                registroNuevo.getDescRegistroIO(),
                registroNuevo.getCantidadIO(),
                registroNuevo.getPrecioUnitarioIO(),
                registroNuevo.getTotalPrecioIO(),
                0,0.0,0.0,
                registroNuevo.getCantidadExistencias(),
                registroNuevo.getPrecioExistencias(),
                registroNuevo.getTotalPrecioExistencias()
            };
            this.tblmdRegistrosKardex.addRow(data);
        }else{
            Object data[] = {
                registroNuevo.getFechaRealizacion(),
                registroNuevo.getTipoMovimiento(),
                registroNuevo.getDescRegistroIO(),
                0,0.0,0.0,
                registroNuevo.getCantidadIO(),
                registroNuevo.getPrecioUnitarioIO(),
                registroNuevo.getTotalPrecioIO(),                    
                registroNuevo.getCantidadExistencias(),
                registroNuevo.getPrecioExistencias(),
                registroNuevo.getTotalPrecioExistencias()
            };
            this.tblmdRegistrosKardex.addRow(data);
        }  
        this.tblRegistrosKardex.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblRegistrosKardex = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        btnAgregarRegistro = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtTotalEntradas = new javax.swing.JTextField();
        txtTotalPrecioEntradas = new javax.swing.JTextField();
        txtTotalSalidas = new javax.swing.JTextField();
        txtTotalPrecioSalidas = new javax.swing.JTextField();
        txtTotalExistencias = new javax.swing.JTextField();
        txtTotalPrecioExistencias = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        labElaboro = new javax.swing.JLabel();
        labReviso = new javax.swing.JLabel();
        labAutorizo = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        labArticulo = new javax.swing.JLabel();
        labCodArticulo = new javax.swing.JLabel();
        labUbicacion = new javax.swing.JLabel();
        labMaximo = new javax.swing.JLabel();
        labMinimo = new javax.swing.JLabel();
        labProduccion = new javax.swing.JLabel();
        labMetodo = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        labCosto = new javax.swing.JLabel();
        labUnitarioAprox = new javax.swing.JLabel();
        txtUnitarioAprox = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Kardex");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        tblRegistrosKardex.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Fecha", "Movimiento", "Descripcion", "Cantidad Entradas", "Precio Unitario", "Total Entradas", "Cantidad Salidas", "Precio Unitario", "Total Salidas", "Cantidad Existencias", "Precio", "Total Existencias"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class, java.lang.Integer.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblRegistrosKardex.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblRegistrosKardex);

        jLabel1.setText("Empresa:");

        btnAgregarRegistro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAgregarRegistro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add.png"))); // NOI18N
        btnAgregarRegistro.setText("Agregar Registro");
        btnAgregarRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarRegistroActionPerformed(evt);
            }
        });

        jLabel2.setText("Artículo:");

        jLabel3.setText("Código:");

        jLabel4.setText("Ubicación:");

        jLabel5.setText("Máximo:");

        jLabel6.setText("Mínimo:");

        jLabel7.setText("Metodo:");

        jLabel8.setText("Total de Entradas(Cantidad):");

        jLabel9.setText("Total de Entradas($):");

        jLabel10.setText("Total de Salidas(Cantidad):");

        jLabel11.setText("Total de Salidas($):");

        txtTotalEntradas.setEditable(false);

        txtTotalPrecioEntradas.setEditable(false);

        txtTotalSalidas.setEditable(false);

        txtTotalPrecioSalidas.setEditable(false);

        txtTotalExistencias.setEditable(false);

        txtTotalPrecioExistencias.setEditable(false);

        jLabel12.setText("Total de Existencias(Cantidad):");

        jLabel13.setText("Total de Existencias($):");

        jLabel14.setText("Elaboró");

        jLabel15.setText("Revisó");

        jLabel16.setText("Autorizó");

        labElaboro.setText("----");

        labReviso.setText("----");

        labAutorizo.setText("----");

        jLabel20.setText("Para Produccion de:");

        labArticulo.setText("----");

        labCodArticulo.setText("----");

        labUbicacion.setText("----");

        labMaximo.setText("----");

        labMinimo.setText("----");

        labProduccion.setText("----");

        labMetodo.setText("----");

        jLabel28.setText("COOPERATIVA DEL POLÍGONO INDUSTRIAL DON BOSCO");

        labCosto.setText("(--------)");

        labUnitarioAprox.setText("----------------------");

        txtUnitarioAprox.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1164, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel5))
                                .addGap(74, 74, 74)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labMaximo)
                                    .addComponent(labArticulo))
                                .addGap(181, 181, 181)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(18, 18, 18)
                                        .addComponent(labCodArticulo))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(labMinimo)))
                                .addGap(271, 271, 271)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel7))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labUbicacion)
                                    .addComponent(labMetodo)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtTotalEntradas, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                                            .addComponent(txtTotalPrecioEntradas, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10)
                                            .addComponent(jLabel11)
                                            .addComponent(labCosto)
                                            .addComponent(labUnitarioAprox)))
                                    .addComponent(labElaboro))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel15)
                                            .addComponent(labReviso))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(labAutorizo)
                                            .addComponent(jLabel16))
                                        .addGap(89, 89, 89))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txtUnitarioAprox, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtTotalSalidas, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                                            .addComponent(txtTotalPrecioSalidas, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel13)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(txtTotalPrecioExistencias, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel12)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtTotalExistencias, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addGap(18, 18, 18)
                                .addComponent(labProduccion))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(69, 69, 69)
                                .addComponent(jLabel28)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAgregarRegistro)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAgregarRegistro)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel28))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel2)
                                .addComponent(labArticulo))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(labCodArticulo)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(labMaximo)
                            .addComponent(labMinimo)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(labUbicacion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(labMetodo))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(labProduccion))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel10)
                    .addComponent(txtTotalEntradas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalSalidas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalExistencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel11)
                    .addComponent(txtTotalPrecioEntradas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalPrecioSalidas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalPrecioExistencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labCosto)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labUnitarioAprox)
                    .addComponent(txtUnitarioAprox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labElaboro)
                    .addComponent(labReviso)
                    .addComponent(labAutorizo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel14)
                    .addComponent(jLabel16))
                .addGap(16, 16, 16))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarRegistroActionPerformed
        // TODO add your handling code here:
        this.registroNuevo = new RegistroIO();
        RegistrarEntradaSalida registro = new RegistrarEntradaSalida(this.kardex, this.inventario, this.registroNuevo);
        
        Dimension desktopSize = this.getParent().getSize();
        Dimension jInternalFrameSize = registro.getSize();
        registro.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 2);
        this.getParent().add(registro);
        registro.show(); 
    }//GEN-LAST:event_btnAgregarRegistroActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        // TODO add your handling code here:
        if(this.registroNuevo != null){
            this.actualizarTablaRegistrosKardex();
            this.llenarBaseTotales();
            this.registroNuevo = null;            
        }        
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarRegistro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labArticulo;
    private javax.swing.JLabel labAutorizo;
    private javax.swing.JLabel labCodArticulo;
    private javax.swing.JLabel labCosto;
    private javax.swing.JLabel labElaboro;
    private javax.swing.JLabel labMaximo;
    private javax.swing.JLabel labMetodo;
    private javax.swing.JLabel labMinimo;
    private javax.swing.JLabel labProduccion;
    private javax.swing.JLabel labReviso;
    private javax.swing.JLabel labUbicacion;
    private javax.swing.JLabel labUnitarioAprox;
    private javax.swing.JTable tblRegistrosKardex;
    private javax.swing.JTextField txtTotalEntradas;
    private javax.swing.JTextField txtTotalExistencias;
    private javax.swing.JTextField txtTotalPrecioEntradas;
    private javax.swing.JTextField txtTotalPrecioExistencias;
    private javax.swing.JTextField txtTotalPrecioSalidas;
    private javax.swing.JTextField txtTotalSalidas;
    private javax.swing.JTextField txtUnitarioAprox;
    // End of variables declaration//GEN-END:variables
}
