/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     17/11/2019 15:10:32                          */
/*==============================================================*/


drop index ALMACENA_FK;

drop index ARTICULO_PK;

drop table ARTICULO;

drop index ES_MATERIAL_DE_FK;

drop index ES_MATERIAL_DE2_FK;

drop index ES_MATERIAL_DE_PK;

drop table ES_MATERIAL_DE;

drop index INCURREN_FK;

drop index GASTOSINDIRECTOS_PK;

drop table GASTOSINDIRECTOS;

drop index INVENTARIO_PK;

drop table INVENTARIO;

drop index PERTENECE_FK;

drop index POSEE_FK;

drop index KARDEX_PK;

drop table KARDEX;

drop index LLEVA_FK;

drop index REGISTROIO_PK;

drop table REGISTROIO;

/*==============================================================*/
/* Table: ARTICULO                                              */
/*==============================================================*/
create table ARTICULO (
   CODIGOARTICULO       VARCHAR(10)          not null,
   IDINVENTARIO         INT4                 null,
   TIPOARTICULO         VARCHAR(30)          null,
   NOMBREARTICULO       VARCHAR(50)          null,
   DESCARTICULO         VARCHAR(150)         null,
   UBICACION            VARCHAR(150)         null,
   PROVEEDOR            VARCHAR(50)          null,
   UNIDADMEDIDA         VARCHAR(15)          null,
   constraint PK_ARTICULO primary key (CODIGOARTICULO)
);

/*==============================================================*/
/* Index: ARTICULO_PK                                           */
/*==============================================================*/
create unique index ARTICULO_PK on ARTICULO (
CODIGOARTICULO
);

/*==============================================================*/
/* Index: ALMACENA_FK                                           */
/*==============================================================*/
create  index ALMACENA_FK on ARTICULO (
IDINVENTARIO
);

/*==============================================================*/
/* Table: ES_MATERIAL_DE                                        */
/*==============================================================*/
create table ES_MATERIAL_DE (
   ART_CODIGOARTICULO   VARCHAR(10)          not null,
   CODIGOARTICULO       VARCHAR(10)          not null,
   constraint PK_ES_MATERIAL_DE primary key (ART_CODIGOARTICULO, CODIGOARTICULO)
);

/*==============================================================*/
/* Index: ES_MATERIAL_DE_PK                                     */
/*==============================================================*/
create unique index ES_MATERIAL_DE_PK on ES_MATERIAL_DE (
ART_CODIGOARTICULO,
CODIGOARTICULO
);

/*==============================================================*/
/* Index: ES_MATERIAL_DE2_FK                                    */
/*==============================================================*/
create  index ES_MATERIAL_DE2_FK on ES_MATERIAL_DE (
CODIGOARTICULO
);

/*==============================================================*/
/* Index: ES_MATERIAL_DE_FK                                     */
/*==============================================================*/
create  index ES_MATERIAL_DE_FK on ES_MATERIAL_DE (
ART_CODIGOARTICULO
);

/*==============================================================*/
/* Table: GASTOSINDIRECTOS                                      */
/*==============================================================*/
create table GASTOSINDIRECTOS (
   IDGIF                SERIAL               not null,
   CODIGOARTICULO       VARCHAR(10)          null,
   CONCEPTO             VARCHAR(50)          null,
   FECHA                DATE                 null,
   DESCGASTOIF          VARCHAR(200)         null,
   VALOR                DECIMAL(10,2)        null,
   constraint PK_GASTOSINDIRECTOS primary key (IDGIF)
);

/*==============================================================*/
/* Index: GASTOSINDIRECTOS_PK                                   */
/*==============================================================*/
create unique index GASTOSINDIRECTOS_PK on GASTOSINDIRECTOS (
IDGIF
);

/*==============================================================*/
/* Index: INCURREN_FK                                           */
/*==============================================================*/
create  index INCURREN_FK on GASTOSINDIRECTOS (
CODIGOARTICULO
);

/*==============================================================*/
/* Table: INVENTARIO                                            */
/*==============================================================*/
create table INVENTARIO (
   IDINVENTARIO         SERIAL               not null,
   NOMBREINVENTARIO     VARCHAR(50)          null,
   DESCINVENTARIO       VARCHAR(200)         null,
   METODOVALUACION      VARCHAR(10)          null,
   TOTALUNIDADES        INT4                 null,
   TOTALPRECIOUNIDADES  DECIMAL(10,2)        null,
   constraint PK_INVENTARIO primary key (IDINVENTARIO)
);

/*==============================================================*/
/* Index: INVENTARIO_PK                                         */
/*==============================================================*/
create unique index INVENTARIO_PK on INVENTARIO (
IDINVENTARIO
);

/*==============================================================*/
/* Table: KARDEX                                                */
/*==============================================================*/
create table KARDEX (
   IDKARDEX             SERIAL               not null,
   CODIGOARTICULO       VARCHAR(10)          null,
   IDINVENTARIO         INT4                 null,
   CANTMAXIMA           INT4                 null,
   CANTMINIMA           INT4                 null,
   TOTALENTRADAS        INT4                 null,
   TOTALSALIDAS         INT4                 null,
   EXISTENCIATOTAL      INT4                 null,
   TOTALPRECIOENTRADAS  DECIMAL(10,2)        null,
   TOTALPRECIOSALIDAS   DECIMAL(10,2)        null,
   PRECIOEXISTENCIATOTAL FLOAT8               null,
   FECHAINICIO          DATE                 null,
   FECHAFIN             DATE                 null,
   ELABORO              VARCHAR(50)          null,
   REVISO               VARCHAR(50)          null,
   AUTORIZO             VARCHAR(50)          null,
   PARAPRODUCCIR        VARCHAR(50)          null,
   constraint PK_KARDEX primary key (IDKARDEX)
);

/*==============================================================*/
/* Index: KARDEX_PK                                             */
/*==============================================================*/
create unique index KARDEX_PK on KARDEX (
IDKARDEX
);

/*==============================================================*/
/* Index: POSEE_FK                                              */
/*==============================================================*/
create  index POSEE_FK on KARDEX (
CODIGOARTICULO
);

/*==============================================================*/
/* Index: PERTENECE_FK                                          */
/*==============================================================*/
create  index PERTENECE_FK on KARDEX (
IDINVENTARIO
);

/*==============================================================*/
/* Table: REGISTROIO                                            */
/*==============================================================*/
create table REGISTROIO (
   IDREGISTROIO         SERIAL               not null,
   IDKARDEX             INT4                 null,
   TIPOMOVIMIENTO       VARCHAR(100)         null,
   TIPOREGISTRO         VARCHAR(10)          null,
   FECHAREALIZACION     DATE                 null,
   DESCREGISTRO         VARCHAR(200)         null,
   CANTIDADIO           INT4                 null,
   PRECIOUNITARIOIO     DECIMAL(10,2)        null,
   TOTALPRECIOIO        DECIMAL(10,2)        null,
   CANTIDADEXISTENCIAS  INT4                 null,
   PRECIOEXISTENCIAS    DECIMAL(10,2)        null,
   TOTALPRECIOEXISTENCIAS DECIMAL(10,2)        null,
   constraint PK_REGISTROIO primary key (IDREGISTROIO)
);

/*==============================================================*/
/* Index: REGISTROIO_PK                                         */
/*==============================================================*/
create unique index REGISTROIO_PK on REGISTROIO (
IDREGISTROIO
);

/*==============================================================*/
/* Index: LLEVA_FK                                              */
/*==============================================================*/
create  index LLEVA_FK on REGISTROIO (
IDKARDEX
);

alter table ARTICULO
   add constraint FK_ARTICULO_ALMACENA_INVENTAR foreign key (IDINVENTARIO)
      references INVENTARIO (IDINVENTARIO)
      on delete restrict on update restrict;

alter table ES_MATERIAL_DE
   add constraint FK_ES_MATER_ES_MATERI_ARTICULO foreign key (ART_CODIGOARTICULO)
      references ARTICULO (CODIGOARTICULO)
      on delete restrict on update restrict;

alter table ES_MATERIAL_DE
   add constraint FK_ES_MATER_ES_MATERI_ARTICULO2 foreign key (CODIGOARTICULO)
      references ARTICULO (CODIGOARTICULO)
      on delete restrict on update restrict;

alter table GASTOSINDIRECTOS
   add constraint FK_GASTOSIN_INCURREN_ARTICULO foreign key (CODIGOARTICULO)
      references ARTICULO (CODIGOARTICULO)
      on delete restrict on update restrict;

alter table KARDEX
   add constraint FK_KARDEX_PERTENECE_INVENTAR foreign key (IDINVENTARIO)
      references INVENTARIO (IDINVENTARIO)
      on delete restrict on update restrict;

alter table KARDEX
   add constraint FK_KARDEX_POSEE_ARTICULO foreign key (CODIGOARTICULO)
      references ARTICULO (CODIGOARTICULO)
      on delete restrict on update restrict;

alter table REGISTROIO
   add constraint FK_REGISTRO_LLEVA_KARDEX foreign key (IDKARDEX)
      references KARDEX (IDKARDEX)
      on delete restrict on update restrict;

INSERT INTO public.inventario(
	idinventario, nombreinventario, descinventario, metodovaluacion, totalunidades, totalpreciounidades)
	VALUES (1, 'Inventario de Productos', 'Almacena los productos terminados y listos  para la venta', 'CPP', 0, 0.00);
INSERT INTO public.inventario(
	idinventario, nombreinventario, descinventario, metodovaluacion, totalunidades, totalpreciounidades)
	VALUES (2, 'Inventario de Productos en Prodceso', 'Almacena los productos y materiales que estan en el area de produccion', 'CPP', 0, 0.00);
INSERT INTO public.inventario(
	idinventario, nombreinventario, descinventario, metodovaluacion, totalunidades, totalpreciounidades)
	VALUES (3, 'Inventario de Materiales', 'Almacena los materiales que estan en bodega', 'CPP', 0, 0.00);