package BDConexion;

import static java.awt.Frame.NORMAL;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import Clases.*;
import Clases.GastosIndirectos.GastoIndirecto;
import Clases.Inventarios.Articulo;
import Clases.Inventarios.Inventario;
import Clases.Inventarios.Kardex;
import Clases.Inventarios.RegistroIO;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class Conexion {

    private Connection conn = null;
    String barra = File.separator;
    String proyecto = System.getProperty("user.dir") + barra + "Registros";

    public Connection AbrirBD() {
        try {
            Class.forName("org.postgresql.Driver");

            try {
                String url = "jdbc:postgresql://db-postgresql-nyc1-38326-do-user-6678417-0.db.ondigitalocean.com:25060/sic";
                Properties props = new Properties();
                props.setProperty("user", "doadmin");
                props.setProperty("password", "az51r8qv9iaikoa9");
                props.setProperty("sslmode ", "require");
                conn = DriverManager.getConnection(url, props);
                /*Esto es porque al hacer mi parte trabaje en una base local*/
                /*String url = "jdbc:postgresql://localhost:5432/inventarios";
                Properties props = new Properties();
                props.setProperty("user", "postgres");
                props.setProperty("password", "postgres");
                props.setProperty("sslmode ", "require");
                conn = DriverManager.getConnection(url, props);
                System.out.println("Base Cargada");*/
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("NO SE PUDO CONECTAR");
            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("NO SE PUDO CONECTAR CON LA BASE DE DATOS");
        }
        return conn;
    }

    public void CerrarBD() {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList listaCatalago() {
        ArrayList<Catalogo> lista = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT IDCUENTA, NOMBRE FROM CATALOGO ORDER BY IDCUENTA ASC");
            while (rs.next()) {
                Catalogo catalogo = new Catalogo();
                catalogo.setIdCuenta(rs.getInt(1));
                catalogo.setNombre(rs.getString(2));
                lista.add(catalogo);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return lista;
    }

    public int idenTransaccion() {
        int identificador = 999;
        ArrayList<Catalogo> lista = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT IDENTIFICADOR FROM "
                    + "TRANSACCION ORDER BY IDENTIFICADOR DESC LIMIT 1");
            if (rs.next()) {
                identificador = rs.getInt(1);
                System.out.println("IDENTIFICADOR " + identificador);
            }
            stat.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return identificador;
    }

    public String agregarTransaccion(ArrayList<Transaccion> insertar) {
        String res = "";
        int identificador = idenTransaccion();
        identificador += 1;
        AbrirBD();
        for (Transaccion t : insertar) {
            try {
                Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
                PreparedStatement pr = conn.prepareStatement("INSERT INTO "
                        + "TRANSACCION(IDENTIFICADOR, IDCUENTA, "
                        + "DEBE, HABER, IVACREDITO, IVADEBITO) "
                        + "VALUES (?,?,?,?,?,?)");
                pr.setInt(1, identificador);
                pr.setInt(2, t.getIdCuenta());
                pr.setDouble(3, t.getDebe());
                pr.setDouble(4, t.getHaber());
                pr.setDouble(5, t.getIvacredito());
                pr.setDouble(6, t.getIvadebito());
                pr.executeUpdate();
                stat.close();
                res = "Registros Ingresados";
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
                res = "Ocurrio un error";
            }
        }
        CerrarBD();
        return res;
    }

    public ArrayList listadoTransacciones() {
        ArrayList<Transaccion> listado = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT T.IDENTIFICADOR, "
                    + "C.NOMBRE, T.DEBE, T.HABER, T.IVACREDITO, "
                    + "T.IVADEBITO FROM TRANSACCION AS T "
                    + "INNER JOIN CATALOGO AS C "
                    + "ON C.IDCUENTA = T.IDCUENTA ORDER BY T.IDENTIFICADOR ASC");
            while (rs.next()) {
                Transaccion campo = new Transaccion();
                campo.setIdCuenta(rs.getInt(1));
                campo.setNombreCuenta(rs.getString(2));
                campo.setDebe(rs.getDouble(3));
                campo.setHaber(rs.getDouble(4));
                campo.setIvacredito(rs.getDouble(5));
                campo.setIvadebito(rs.getDouble(6));
                listado.add(campo);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return listado;
    }

    public String eliminarRegistro(String identificador) {
        AbrirBD();
        String valor = "";
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            PreparedStatement pr = conn.prepareStatement("DELETE FROM "
                    + "TRANSACCION WHERE IDENTIFICADOR = " + identificador + "");
            pr.executeUpdate();
            pr.close();
            stat.close();
            valor = "Registros Eliminados";
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            valor = "Ocurrio un error";
        }
        CerrarBD();
        return valor;
    }

    public String cerrarCiclo() {
        String resultado = "Ocurrio un error";
        AbrirBD();

        ArrayList<Transaccion> sumarizacion = new ArrayList<>();
        double acreditable = 0;
        double retenido = 0;

        //Primero la suma del IVA
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT SUM(ivacredito), "
                    + "SUM(ivadebito) FROM transaccion");
            while (rs.next()) {
                acreditable += rs.getDouble(1);
                retenido += rs.getDouble(2);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        //Segundo la suma del DEBE y HABER de cada cuenta
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT idcuenta, SUM(DEBE), "
                    + "SUM(HABER) FROM TRANSACCION GROUP BY IDCUENTA "
                    + "ORDER BY IDCUENTA ASC");
            while (rs.next()) {
                Transaccion catalogo = new Transaccion();
                catalogo.setIdCuenta(rs.getInt(1));
                catalogo.setDebe(rs.getDouble(2));
                catalogo.setHaber(rs.getDouble(3));
                sumarizacion.add(catalogo);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        //Se elimina lo que se encuentre en la tabla CICLOCERRADO
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            PreparedStatement pr1 = conn.prepareStatement("DELETE FROM CICLOCERRADO WHERE IDCERRAR > 0");
            pr1.execute();
            pr1.close();
            stat.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Se inserta la SUMARIZACION por cuenta
        for (Transaccion t : sumarizacion) {
            try {
                Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
                PreparedStatement pr = conn.prepareStatement("INSERT INTO "
                        + "CICLOCERRADO(IDCERRAR,IDCUENTA, "
                        + "SUMDEBE, SUMHABER) "
                        + "VALUES (?,?,?,?)");
                pr.setInt(1, 1);
                pr.setInt(2, t.getIdCuenta());

                double md, mh;
                md = t.getDebe();
                mh = t.getHaber();
                if (Double.compare(md, mh) == 0) {
                    t.setDebe(0);
                    t.setHaber(0);
                }
                if (md > mh) {
                    t.setDebe(md - mh);
                    t.setHaber(0);
                } else {
                    t.setDebe(0);
                    t.setHaber(mh - md);
                }

                pr.setDouble(3, t.getDebe());
                pr.setDouble(4, t.getHaber());

                pr.executeUpdate();
                pr.close();
                stat.close();
                resultado = "Ciclo cerrado Exitosamente";
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //Se inserta el Resultado del IVA 
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            PreparedStatement pr = conn.prepareStatement("INSERT INTO "
                    + "CICLOCERRADO(IDCERRAR,IDCUENTA, "
                    + "SUMDEBE, SUMHABER) "
                    + "VALUES (?,?,?,?)");
            pr.setInt(1, 1);
            pr.setInt(2, 777777);

            if (Double.compare(acreditable, retenido) == 0) {
                pr.setDouble(3, 0);
                pr.setDouble(4, 0);
            }
            if (acreditable > retenido) {
                pr.setDouble(3, acreditable - retenido);
                pr.setDouble(4, 0);
            } else {
                pr.setDouble(3, 0);
                pr.setDouble(4, retenido - acreditable);
            }
            pr.executeUpdate();
            pr.close();
            stat.close();
            resultado = "Ciclo cerrado Exitosamente";
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            resultado = "Ocurrio un erro al Cerrar Ciclo";
        }
        CerrarBD();
        return resultado;
    }

    public ArrayList estadosFinancieros() {
        AbrirBD();
        ArrayList<Transaccion> sumarizacion = new ArrayList<>();

        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT F.IDCUENTA, "
                    + "F.SUMDEBE, F.SUMHABER, C.NOMBRE FROM CICLOCERRADO "
                    + "AS F INNER JOIN CATALOGO AS C ON F.IDCUENTA = C.IDCUENTA "
                    + "ORDER BY c.idcuenta ASC");
            while (rs.next()) {
                Transaccion catalogo = new Transaccion();
                catalogo.setIdCuenta(rs.getInt(1));
                catalogo.setDebe(rs.getDouble(2));
                catalogo.setHaber(rs.getDouble(3));
                catalogo.setNombreCuenta(rs.getString(4));
                sumarizacion.add(catalogo);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        CerrarBD();
        return sumarizacion;
    }

    public String manoObra(Empleado e) {
        AbrirBD();
        String res = "";
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            PreparedStatement pr = conn.prepareStatement("INSERT INTO "
                    + "EMPLEADO(nombre, cargo, septimo, aguinaldo, "
                    + "vacaciones, isss, afp, insaforp, fr) "
                    + "VALUES (?,?,?,?,?,?,?,?,?)");
            pr.setString(1, e.getNombre());
            pr.setString(2, e.getCargo());
            pr.setDouble(3, e.getDesembolo_sem());
            pr.setDouble(4, e.getAguinaldo());
            pr.setDouble(5, e.getVacaciones());
            pr.setDouble(6, e.getIsss());
            pr.setDouble(7, e.getAfp());
            pr.setDouble(8, e.getInsaforp());
            pr.setDouble(9, e.getFactor_recargo());
            pr.executeUpdate();
            pr.close();
            stat.close();
            res = "Registros Ingresados";
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            res = "Ocurrio un error";
        }
        CerrarBD();
        return res;
    }

    public ArrayList listadoMano() {
        ArrayList<Empleado> listado = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT nombre, cargo, septimo, aguinaldo, vacaciones, isss, afp, insaforp, fr FROM empleado ORDER BY idempleado ASC");
            while (rs.next()) {
                Empleado e = new Empleado();
                e.setNombre(rs.getString(1));
                e.setCargo(rs.getString(2));
                e.setSeptimo(rs.getDouble(3));
                e.setAguinaldo(rs.getDouble(4));
                e.setVacaciones(rs.getDouble(5));
                e.setIsss(rs.getDouble(6));
                e.setAfp(rs.getDouble(7));
                e.setInsaforp(rs.getDouble(8));
                e.setFactor_recargo(rs.getDouble(9));
                listado.add(e);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return listado;
    }

    public ArrayList EstadoResultado() {
        ArrayList<Transaccion> listado = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT "
                    + "c.idcuenta, c.nombre, r.sumdebe, r.sumhaber "
                    + "FROM ciclocerrado AS r "
                    + "INNER JOIN catalogo AS c "
                    + "USING (idcuenta) "
                    + "WHERE idcuenta >= 400000 AND idcuenta < 600000 "
                    + "ORDER BY idcuenta ASC");
            while (rs.next()) {
                Transaccion e = new Transaccion();
                e.setIdCuenta(rs.getInt(1));
                e.setNombreCuenta(rs.getString(2));
                e.setDebe(rs.getDouble(3));
                e.setHaber(rs.getDouble(4));
                listado.add(e);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return listado;
    }

    public ArrayList EstadoCapital() {
        ArrayList<Transaccion> listado = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT "
                    + "c.idcuenta, c.nombre, r.sumdebe, r.sumhaber "
                    + "FROM ciclocerrado AS r "
                    + "INNER JOIN catalogo AS c "
                    + "USING (idcuenta) "
                    + "WHERE idcuenta >= 300000 AND idcuenta < 400000 "
                    + "ORDER BY idcuenta ASC");
            while (rs.next()) {
                Transaccion e = new Transaccion();
                e.setIdCuenta(rs.getInt(1));
                e.setNombreCuenta(rs.getString(2));
                e.setDebe(rs.getDouble(3));
                e.setHaber(rs.getDouble(4));
                listado.add(e);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return listado;
    }

    public ArrayList Activos() {
        ArrayList<Transaccion> listado = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT "
                    + "c.idcuenta, c.nombre, r.sumdebe, r.sumhaber "
                    + "FROM ciclocerrado AS r "
                    + "INNER JOIN catalogo AS c "
                    + "USING (idcuenta) "
                    + "WHERE idcuenta >= 100000 AND idcuenta < 200000 "
                    + "ORDER BY idcuenta ASC");
            while (rs.next()) {
                Transaccion e = new Transaccion();
                e.setIdCuenta(rs.getInt(1));
                e.setNombreCuenta(rs.getString(2));
                e.setDebe(rs.getDouble(3));
                e.setHaber(rs.getDouble(4));
                listado.add(e);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return listado;
    }

    public ArrayList Pasivos() {
        ArrayList<Transaccion> listado = new ArrayList<>();
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT "
                    + "c.idcuenta, c.nombre, r.sumdebe, r.sumhaber "
                    + "FROM ciclocerrado AS r "
                    + "INNER JOIN catalogo AS c "
                    + "USING (idcuenta) "
                    + "WHERE idcuenta >= 200000 AND idcuenta < 300000 "
                    + "ORDER BY idcuenta ASC");
            while (rs.next()) {
                Transaccion e = new Transaccion();
                e.setIdCuenta(rs.getInt(1));
                e.setNombreCuenta(rs.getString(2));
                e.setDebe(rs.getDouble(3));
                e.setHaber(rs.getDouble(4));
                listado.add(e);
            }
            stat.close();
            rs.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return listado;
    }

    public double utilidad() {
        double utilidad = 0.0;
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT utilidad()");
            if (rs.next()) {
                utilidad = rs.getDouble(1);
                System.out.println("Utilidad " + utilidad);
            }
            stat.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return utilidad;
    }

    public double capital() {
        double inversion = 0.0;
        double desinversion = 0.0;
        double total = 0.0;
        AbrirBD();
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT inversion()");

            if (rs.next()) {
                inversion = rs.getDouble(1);
                System.out.println("Inversion " + inversion);
            }

            rs = stat.executeQuery("SELECT desinversion()");
            if (rs.next()) {
                desinversion = rs.getDouble(1);
                System.out.println("Desinversion " + desinversion);
            }

            stat.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        total = inversion - desinversion;
        System.out.println("TOTAL CAPITAL: " + total);
        return total;
    }

    public String agregarFecha(String fecha) {
        AbrirBD();
        String res = "";

        try {
            Statement stat1 = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            PreparedStatement pr1 = conn.prepareStatement("DELETE FROM "
                    + "fechacontable WHERE idfecha > 0");
            pr1.executeUpdate();
            pr1.close();
            stat1.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            PreparedStatement pr = conn.prepareStatement("INSERT INTO "
                    + "fechacontable(idfecha, fecha) "
                    + "VALUES (?,?)");
            pr.setInt(1, 1);
            pr.setString(2, fecha);
            pr.executeUpdate();
            pr.close();
            stat.close();
            res = "Ciclo Cerrado";
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            res = "Ocurrio un error";
        }
        CerrarBD();
        return res;
    }

    public String obtenerFecha() {
        AbrirBD();
        String res = "Fecha no agregada";
        try {
            Statement stat = conn.createStatement(HIDE_ON_CLOSE, NORMAL);
            ResultSet rs = stat.executeQuery("SELECT fecha FROM fechacontable");
            if (rs.next()) {
                res = rs.getString(1);                
            }
            stat.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        CerrarBD();
        return res;
    }
    
    
    /****************************************************************************/
    /*                                                                          */
    /*                                                                          */
    /*Parte de los inventarios                                                  */
    /*                                                                          */
    /*                                                                          */
    /****************************************************************************/
    /*Acceso a tabla Inventario*/
    public ArrayList<Inventario> listarInventarios(){       
        ArrayList<Inventario> listaInventarios = new ArrayList<Inventario>();
        String query = "select * from inventario order by(idinventario) asc";
        
        AbrirBD();
        try{
            Statement statement = this.conn.createStatement();
            ResultSet resultado = statement.executeQuery(query);
            
            while(resultado.next()){
                Inventario inventario = new Inventario();
                inventario.setIdInventario(resultado.getInt("idinventario"));
                inventario.setNombreInventario(resultado.getString("nombreinventario"));
                inventario.setDescInventario(resultado.getString("descinventario"));
                inventario.setMetodoValuacion(resultado.getString("metodovaluacion"));
                inventario.setTotalUnidades(resultado.getInt("totalunidades"));
                inventario.setTotalPrecioUnidades(resultado.getFloat("totalpreciounidades"));
                
                listaInventarios.add(inventario);
            }

            statement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al extraer los inventarios de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return listaInventarios;
    }
    
    public Inventario obtenerInventarioPorId(int idInventario){  
        Inventario inventario = null;
        String query = "select * from inventario where idInventario = " + idInventario;
        
        AbrirBD();
        try{
            Statement statement = this.conn.createStatement();
            ResultSet resultado = statement.executeQuery(query);
            
            resultado.next();
            inventario = new Inventario();
            inventario.setIdInventario(resultado.getInt("idinventario"));
            inventario.setNombreInventario(resultado.getString("nombreinventario"));
            inventario.setDescInventario(resultado.getString("descinventario"));
            inventario.setMetodoValuacion(resultado.getString("metodovaluacion"));
            inventario.setTotalUnidades(resultado.getInt("totalunidades"));
            inventario.setTotalPrecioUnidades(resultado.getFloat("totalpreciounidades"));

            statement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al extraer el inventario de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return inventario;
        
    }
    
    public void actualizarSumasInventario(Inventario inventario){
        String query = "update inventario set "
                + "totalunidades = ?, totalpreciounidades = ? where idinventario = ?";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);
            pstatement.setInt(1, inventario.getTotalUnidades());
            pstatement.setFloat(2, inventario.getTotalPrecioUnidades());
            pstatement.setInt(3, inventario.getIdInventario());
            
            pstatement.executeUpdate();

            pstatement.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al ingresar las sumas de inventarios en la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();        
    }
    
    /*acceso a tabla articulo*/
    public ArrayList<Articulo> listarArticulosPorInventario(int idInventario){
        ArrayList<Articulo> listaArticulos = new ArrayList<Articulo>();
        String query = "select * from articulo where idinventario = ?";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);
            pstatement.setInt(1, idInventario);
            ResultSet resultado = pstatement.executeQuery();
            
            while(resultado.next()){
                Articulo articulo = new Articulo();
                articulo.setCodigoArticulo(resultado.getString("codigoarticulo"));
                articulo.setTipoArticulo(resultado.getString("tipoarticulo"));
                articulo.setNombreArticulo(resultado.getString("nombrearticulo"));
                articulo.setDescArticulo(resultado.getString("descarticulo"));
                articulo.setUbicacion(resultado.getString("ubicacion"));
                articulo.setProveedor(resultado.getString("proveedor"));
                articulo.setUnidadMedida(resultado.getString("unidadmedida"));
                
                listaArticulos.add(articulo);
            }

            pstatement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar los artÃ­culos de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return listaArticulos;
        
    }
    
    public Articulo obtenerArticuloPorCodigo(String codigoArticulo){
        Articulo articulo = new Articulo();
        String query = "select * from articulo where codigoarticulo = ?";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);
            pstatement.setString(1, codigoArticulo);
            ResultSet resultado = pstatement.executeQuery();
            
            resultado.next();
            articulo.setCodigoArticulo(resultado.getString("codigoarticulo"));            
            articulo.setTipoArticulo(resultado.getString("tipoarticulo"));
            articulo.setNombreArticulo(resultado.getString("nombrearticulo"));
            articulo.setDescArticulo(resultado.getString("descarticulo"));
            articulo.setUbicacion(resultado.getString("ubicacion"));
            articulo.setProveedor(resultado.getString("proveedor"));
            articulo.setUnidadMedida(resultado.getString("unidadmedida"));
                    
            pstatement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar el artÃ­culo de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return articulo;                
    }
    
    public ArrayList<String> obtenerNombresProductos(){
        ArrayList<String> productos = new ArrayList<String>();
        String query = "select nombrearticulo from articulo where tipoarticulo = 'PRODUCTO'";
        
        AbrirBD();
        try{            
            Statement statement = this.conn.createStatement();
            ResultSet resultado = statement.executeQuery(query);
            
            while(resultado.next()){
                String prod = resultado.getString("nombrearticulo");
                productos.add(prod);
            }
                    
            statement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar los nombres de los artÃ­culos de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return productos;        
    }
        
    public void agregarArticulo(Articulo articulo, int idInventario){
        String query = "insert into articulo(codigoarticulo,idinventario,tipoarticulo,nombrearticulo,descarticulo,"
                + "ubicacion,proveedor,unidadmedida) values(?,?,?,?,?,?,?,?)";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);
            pstatement.setString(1, articulo.getCodigoArticulo());
            pstatement.setInt(2, idInventario);            
            pstatement.setString(3,articulo.getTipoArticulo());
            pstatement.setString(4,articulo.getNombreArticulo());
            pstatement.setString(5,articulo.getDescArticulo());
            pstatement.setString(6,articulo.getUbicacion());
            pstatement.setString(7,articulo.getProveedor());
            pstatement.setString(8,articulo.getUnidadMedida());
            
            pstatement.executeUpdate();    
            pstatement.close();            
            javax.swing.JOptionPane.showMessageDialog(null, "El artÃ­culo se creo correctamente.", "Registro Ingresado", javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al insertar el artÃ­culo en la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
    }
    
    public void actualizarArticulo(Articulo articulo){        
        String query = "update articulo set nombrearticulo = ?, descarticulo = ?, ubicacion = ?, proveedor = ?,"
                + "unidadmedida = ? where codigoarticulo = ?";
                
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);                        
            
            pstatement.setString(1,articulo.getNombreArticulo());            
            pstatement.setString(2,articulo.getDescArticulo());
            pstatement.setString(3,articulo.getUbicacion());
            pstatement.setString(4,articulo.getProveedor());
            pstatement.setString(5,articulo.getUnidadMedida());
            pstatement.setString(6,articulo.getCodigoArticulo());
            
            pstatement.executeUpdate();    
            pstatement.close();  
            javax.swing.JOptionPane.showMessageDialog(null, "El artÃ­culo se actualizo correctamente.", "ActualizaciÃ³n completada", javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al actualizar el articulo de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
    }
    
    public int obtenerTotalEntradasPorFechaCodigo(int mes, int year, String codigoArticulo){
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaIniBusq = new Date();
        Date fechaFinBusq = new Date();   
        int total = 0;
        String query = "select sum(kar.totalentradas) as sumaentradas from articulo as art "
                + "inner join kardex as kar on art.codigoarticulo = kar.codigoarticulo "
                + "where kar.fechainicio >= ? and kar.fechainicio <= ? "
                + "and codigoarticulo = ?";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = null; 
            ResultSet resultado = null;            
            
            if(codigoArticulo != "N/A"){
                if(mes != -1){                
                    fechaIniBusq = new Date(year - 1900, mes, 1);         
                    if(mes == 11){
                        fechaFinBusq = new Date(year - 1900, mes, 31);
                        query = "select sum(kar.totalentradas) as sumaentradas from articulo as art "
                                + "inner join kardex as kar "
                                + "on art.codigoarticulo = kar.codigoarticulo "
                                + "where kar.fechainicio >= ? and kar.fechainicio <= ? "
                                + "and codigoarticulo = ?";
                    }else{
                        fechaFinBusq = new Date(year - 1900, mes + 1, 1);
                        query = "select sum(kar.totalentradas) as sumaentradas from articulo as art "
                                + "inner join kardex as kar "
                                + "on art.codigoarticulo = kar.codigoarticulo "
                                + "where kar.fechainicio >= ? and kar.fechainicio < ? "
                                + "and kar.codigoarticulo = ?";
                    }

                    pstatement = this.conn.prepareStatement(query);
                    pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));
                    pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));
                    pstatement.setString(3, codigoArticulo);

                }else{
                    if(mes == -1){
                        fechaIniBusq = new Date(year - 1900, 0, 1);
                        fechaFinBusq = new Date(year - 1900, 11, 31);
                        query = "select sum(kar.totalentradas) as sumaentradas from articulo as art "
                                + "inner join kardex as kar on art.codigoarticulo = kar.codigoarticulo "
                                + "where kar.fechainicio >= ? and kar.fechainicio <= ? "
                                + "and kar.codigoarticulo = ?";

                        pstatement = this.conn.prepareStatement(query);
                        pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));
                        pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));
                        pstatement.setString(3, codigoArticulo);
                    }
                }                
                resultado = pstatement.executeQuery();
                resultado.next();
                total = resultado.getInt("sumaentradas");

                resultado.close();            
                pstatement.close();            
            }
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al obtener la suma de entradas de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return total;
    }
    
    /*Acceso a tabla kardex*/
    public ArrayList<Kardex> obtenerKardexPorArticulo(String codigoArticulo){
        ArrayList<Kardex> kardexs = new ArrayList<Kardex>();
        String query = "select * from kardex where codigoarticulo = ? order by(fechainicio) asc";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);
            pstatement.setString(1, codigoArticulo);
            ResultSet resultado = pstatement.executeQuery();
            
            while(resultado.next()){
                Kardex kardex = new Kardex();
                kardex.setIdKardex(resultado.getInt("idkardex"));
                kardex.setCantMaxima(resultado.getInt("cantmaxima"));
                kardex.setCantMinima(resultado.getInt("cantminima"));
                kardex.setTotalEntradas(resultado.getInt("totalentradas"));
                kardex.setTotalSalidas(resultado.getInt("totalsalidas"));
                kardex.setExistenciaTotal(resultado.getInt("existenciatotal"));
                kardex.setTotalPrecioEntradas(resultado.getFloat("totalprecioentradas"));
                kardex.setTotalPrecioSalidas(resultado.getFloat("totalpreciosalidas"));
                kardex.setPrecioExistenciaTotal(resultado.getFloat("precioexistenciatotal"));
                kardex.setFechaInicio(resultado.getDate("fechainicio"));
                kardex.setFechaFin(resultado.getDate("fechafin"));
                kardex.setElaboro(resultado.getString("elaboro"));
                kardex.setReviso(resultado.getString("reviso"));
                kardex.setAutorizo(resultado.getString("autorizo"));
                kardex.setParaProducir(resultado.getString("paraproduccir"));
                
                kardexs.add(kardex);
            }
                    
            pstatement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar los kardexs de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return kardexs;                
    }
    
    public Kardex obtenerKardexPorId(int idKardex){
        Kardex kardex = null;
        String query = "select * from kardex where idkardex = ?";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);
            pstatement.setInt(1, idKardex);
            ResultSet resultado = pstatement.executeQuery();
            
            resultado.next();
            kardex = new Kardex();
            kardex.setIdKardex(resultado.getInt("idkardex"));
            kardex.setCantMaxima(resultado.getInt("cantmaxima"));
            kardex.setCantMinima(resultado.getInt("cantminima"));
            kardex.setTotalEntradas(resultado.getInt("totalentradas"));
            kardex.setTotalSalidas(resultado.getInt("totalsalidas"));
            kardex.setExistenciaTotal(resultado.getInt("existenciatotal"));
            kardex.setTotalPrecioEntradas(resultado.getFloat("totalprecioentradas"));
            kardex.setTotalPrecioSalidas(resultado.getFloat("totalpreciosalidas"));
            kardex.setPrecioExistenciaTotal(resultado.getFloat("precioexistenciatotal"));
            kardex.setFechaInicio(resultado.getDate("fechainicio"));
            kardex.setFechaFin(resultado.getDate("fechafin"));
            kardex.setElaboro(resultado.getString("elaboro"));
            kardex.setReviso(resultado.getString("reviso"));
            kardex.setAutorizo(resultado.getString("autorizo"));
            kardex.setParaProducir(resultado.getString("paraproduccir"));

            pstatement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar el kardex de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return kardex; 
    }
    
    public int crearKardex(Kardex kardex, Inventario inventario){
        int idGenerado = 0;
        String query = "INSERT INTO kardex "
                + "(codigoarticulo, idinventario, cantmaxima, cantminima, totalentradas, totalsalidas, "
                + "existenciatotal, totalprecioentradas, totalpreciosalidas, precioexistenciatotal, fechainicio, "
                + "fechafin, elaboro, reviso, autorizo, paraproduccir) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            
            pstatement.setString(1, kardex.getArticulo().getCodigoArticulo());
            pstatement.setInt(2, inventario.getIdInventario());
            pstatement.setInt(3, kardex.getCantMaxima());
            pstatement.setInt(4, kardex.getCantMinima());
            pstatement.setInt(5, kardex.getTotalEntradas());
            pstatement.setInt(6, kardex.getTotalSalidas());
            pstatement.setInt(7, kardex.getExistenciaTotal());            
            pstatement.setFloat(8, kardex.getTotalPrecioEntradas());
            pstatement.setFloat(9, kardex.getTotalPrecioSalidas());
            pstatement.setFloat(10, kardex.getPrecioExistenciaTotal());
            
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");            
            pstatement.setDate(11, java.sql.Date.valueOf(dateformat.format(kardex.getFechaInicio())));
            pstatement.setDate(12, java.sql.Date.valueOf(dateformat.format(kardex.getFechaFin())));
            
            pstatement.setString(13, kardex.getElaboro());
            pstatement.setString(14, kardex.getReviso());
            pstatement.setString(15, kardex.getAutorizo());
            pstatement.setString(16, kardex.getParaProducir());
            
            pstatement.executeUpdate();                                    
            ResultSet resultado = pstatement.getGeneratedKeys();//.executeUpdate();
            resultado.next();
            idGenerado = resultado.getInt(1);
            
            pstatement.close();   
            resultado.close();
            javax.swing.JOptionPane.showMessageDialog(null, "El kardex se creo correctamente.", "Registro Ingresado", javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al ingresar el kardex de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return idGenerado;
    }
    
    public void actualizarSumasKardex(Kardex kardex){
        String query = "update kardex set totalentradas = ?, totalsalidas = ?, existenciatotal = ?, "
                + "totalprecioentradas = ?, totalpreciosalidas = ?, precioexistenciatotal = ? "
                + "where idkardex = ?";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = this.conn.prepareStatement(query);
            
            pstatement.setInt(1, kardex.getTotalEntradas());
            pstatement.setInt(2, kardex.getTotalSalidas());
            pstatement.setInt(3, kardex.getExistenciaTotal());            
            pstatement.setFloat(4, kardex.getTotalPrecioEntradas());
            pstatement.setFloat(5, kardex.getTotalPrecioSalidas());
            pstatement.setFloat(6, kardex.getPrecioExistenciaTotal());
            pstatement.setInt(7, kardex.getIdKardex());
            
            pstatement.executeUpdate();
            pstatement.close();               
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al actualizar el kardex.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
    }
    
    public ArrayList<Kardex> obtenerKardexPorFechaInicioProduccion(int mes, int year, String produccion, String codigoArticulo){
        ArrayList<Kardex> kardexs = new ArrayList<Kardex>();
        String query = "";//select * from kardex where codigoarticulo = ?";
        
        AbrirBD();
        try{
            PreparedStatement pstatement = null; 
            ResultSet resultado = null;
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            
            if(mes != -1 && produccion != "N/A"){                
                Date fechaIniBusq = new Date(year - 1900, mes, 1);                
                query = "select * from kardex where fechainicio = ? and paraproduccir = ? and codigoarticulo = ? order by(fechainicio) asc";
                
                pstatement = this.conn.prepareStatement(query);
                pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));
                pstatement.setString(2, produccion);
                pstatement.setString(3, codigoArticulo);
                
            }else{
                if(mes == -1 && produccion != "N/A"){
                    Date fechaIniBusq = new Date(year - 1900, 0, 1);
                    Date fechaFinBusq = new Date(year - 1900, 11, 1);
                    query = "select * from kardex where fechainicio >= ? and fechainicio <= ? and paraproduccir = ? and codigoarticulo = ? order by(fechainicio) asc";
                    
                    pstatement = this.conn.prepareStatement(query);
                    pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));
                    pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));
                    pstatement.setString(3, produccion);
                    pstatement.setString(4, codigoArticulo);
                }else{
                    if(mes != -1 && produccion == "N/A"){
                        Date fechaIniBusq = new Date(year - 1900, mes, 1);
                        query = "select * from kardex where fechainicio = ? and codigoarticulo = ? order by(fechainicio) asc";

                        pstatement = this.conn.prepareStatement(query);
                        pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));    
                        pstatement.setString(2, codigoArticulo);
                    }else{
                        if(mes == -1 && produccion == "N/A"){
                            Date fechaIniBusq = new Date(year - 1900, 0, 1);
                            Date fechaFinBusq = new Date(year - 1900, 11, 1);
                            query = "select * from kardex where fechainicio >= ? and fechainicio <= ? and codigoarticulo = ? order by(fechainicio) asc";

                            pstatement = this.conn.prepareStatement(query);
                            pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));                        
                            pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));  
                            pstatement.setString(3, codigoArticulo);
                        }                        
                    }
                }
            }
            
            resultado = pstatement.executeQuery();
            
            while(resultado.next()){
                Kardex kardex = new Kardex();
                kardex.setIdKardex(resultado.getInt("idkardex"));
                kardex.setCantMaxima(resultado.getInt("cantmaxima"));
                kardex.setCantMinima(resultado.getInt("cantminima"));
                kardex.setTotalEntradas(resultado.getInt("totalentradas"));
                kardex.setTotalSalidas(resultado.getInt("totalsalidas"));
                kardex.setExistenciaTotal(resultado.getInt("existenciatotal"));
                kardex.setTotalPrecioEntradas(resultado.getFloat("totalprecioentradas"));
                kardex.setTotalPrecioSalidas(resultado.getFloat("totalpreciosalidas"));
                kardex.setPrecioExistenciaTotal(resultado.getFloat("precioexistenciatotal"));
                kardex.setFechaInicio(resultado.getDate("fechainicio"));
                kardex.setFechaFin(resultado.getDate("fechafin"));
                kardex.setElaboro(resultado.getString("elaboro"));
                kardex.setReviso(resultado.getString("reviso"));
                kardex.setAutorizo(resultado.getString("autorizo"));
                kardex.setParaProducir(resultado.getString("paraproduccir"));
                
                kardexs.add(kardex);
            }
                    
            pstatement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar los kardexs de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return kardexs; 
    
    }
    
    /*Acceso a tabla RegistroIO*/
    public ArrayList<RegistroIO> obtenerRegistrosPorKardex(int idKardex){
        ArrayList<RegistroIO> registros = new ArrayList<RegistroIO>();
        String query = "select * from registroio where idkardex = ? order by(fecharealizacion) asc";
        
        AbrirBD();        
        try{
            PreparedStatement pstatement = conn.prepareStatement(query);
            pstatement.setInt(1, idKardex);                       
            ResultSet resultado = pstatement.executeQuery();
            
            while(resultado.next()){
                RegistroIO reg = new RegistroIO();
                reg.setIdRegistro(resultado.getInt("idregistroio"));
                reg.setTipoMovimiento(resultado.getString("tipomovimiento"));
                reg.setTipoRegistro(resultado.getString("tiporegistro"));
                reg.setFechaRealizacion(resultado.getDate("fecharealizacion"));
                reg.setDescRegistroIO(resultado.getString("descregistro"));
                reg.setCantidadIO(resultado.getInt("cantidadio"));
                reg.setPrecioUnitarioIO(resultado.getFloat("preciounitarioio"));
                reg.setTotalPrecioIO(resultado.getFloat("totalprecioio"));
                reg.setCantidadExistencias(resultado.getInt("cantidadexistencias"));
                reg.setPrecioExistencias(resultado.getFloat("precioexistencias"));
                reg.setTotalPrecioExistencias(resultado.getFloat("totalprecioexistencias"));
                
                registros.add(reg);
            }

            pstatement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar los registros de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        
        return registros;
    }
    
    public void agregarRegistroIO(RegistroIO registro, Kardex kardex){                
        String query = "insert into registroio"
                + "(idkardex,tipomovimiento,tiporegistro,fecharealizacion,descregistro,"
                + "cantidadio,preciounitarioio,totalprecioio,cantidadexistencias,precioexistencias,"
                + "totalprecioexistencias)"
                + " values(?,?,?,?,?,?,?,?,?,?,?)";
        
        AbrirBD();        
        try{
            PreparedStatement pstatement = conn.prepareStatement(query);
            
            pstatement.setInt(1, kardex.getIdKardex());
            pstatement.setString(2, registro.getTipoMovimiento());
            pstatement.setString(3, registro.getTipoRegistro());
            
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            pstatement.setDate(4, java.sql.Date.valueOf(dateformat.format(registro.getFechaRealizacion())));
            
            /*SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");            
            pstatement.setDate(11, java.sql.Date.valueOf(dateformat.format(kardex.getFechaInicio())));
            pstatement.setDate(12, java.sql.Date.valueOf(dateformat.format(kardex.getFechaFin())));*/
            
            pstatement.setString(5, registro.getDescRegistroIO());
            pstatement.setInt(6, registro.getCantidadIO());
            pstatement.setFloat(7, registro.getPrecioUnitarioIO());
            pstatement.setFloat(8, registro.getTotalPrecioIO());
            pstatement.setInt(9, registro.getCantidadExistencias());
            pstatement.setFloat(10, registro.getPrecioExistencias());
            pstatement.setFloat(11, registro.getTotalPrecioExistencias());
            
            pstatement.executeUpdate();            
            
            pstatement.close();           
            javax.swing.JOptionPane.showMessageDialog(null, "El registro se creo correctamente.", "Registro Ingresado", javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al insertar el registro en la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();       
    }
    
    /*Acceso a tabla GastosIndirectos*/
    public void agregarGastoIndirecto(GastoIndirecto gastoIndirecto, String codigoArticulo){
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        String query = "insert into gastosindirectos"
                + "(codigoarticulo,concepto,fecha,descgastoif,valor) "
                + "values(?,?,?,?,?)";
        
        AbrirBD();        
        try{
            PreparedStatement pstatement = conn.prepareStatement(query);
            pstatement.setString(1, codigoArticulo);
            pstatement.setString(2, gastoIndirecto.getConcepto());
            pstatement.setDate(3, java.sql.Date.valueOf(dateformat.format(gastoIndirecto.getFecha())));
            pstatement.setString(4, gastoIndirecto.getDescGastoif());
            pstatement.setFloat(5, gastoIndirecto.getValor());
            
            pstatement.executeUpdate();
            pstatement.close();            
            javax.swing.JOptionPane.showMessageDialog(null, "El registro de gasto indirecto se creo correctamente.", "Registro Ingresado", javax.swing.JOptionPane.INFORMATION_MESSAGE);
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al insertar el registro de gasto en la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
    }
        
    public ArrayList<GastoIndirecto> obtenerGastoIndirectoPorFechaCodigoArticulo(int mes, int year, String codigoArticulo){
        ArrayList<GastoIndirecto> gastos = new ArrayList<GastoIndirecto>();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaIniBusq = new Date();
        Date fechaFinBusq = new Date();        
        String query = "";//select * from kardex where codigoarticulo = ?";
        
        AbrirBD();        
        try{
            PreparedStatement pstatement = null; 
            ResultSet resultado = null;            
            
            if(mes != -1 && codigoArticulo != "N/A"){                
                fechaIniBusq = new Date(year - 1900, mes, 1);         
                if(mes == 11){
                    fechaFinBusq = new Date(year - 1900, mes, 31);
                    query = "select * from gastosindirectos where fecha >= ? and fecha <= ? and codigoarticulo = ? order by(fecha) asc";
                }else{
                    fechaFinBusq = new Date(year - 1900, mes + 1, 1);
                    query = "select * from gastosindirectos where fecha >= ? and fecha < ? and codigoarticulo = ? order by(fecha) asc";
                }
                
                pstatement = this.conn.prepareStatement(query);
                pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));
                pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));
                pstatement.setString(3, codigoArticulo);
                
            }else{
                if(mes == -1 && codigoArticulo != "N/A"){
                    fechaIniBusq = new Date(year - 1900, 0, 1);
                    fechaFinBusq = new Date(year - 1900, 11, 31);
                    query = "select * from gastosindirectos where fecha >= ? and fecha <= ? and codigoarticulo = ? order by(fecha) asc";
                    
                    pstatement = this.conn.prepareStatement(query);
                    pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));
                    pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));
                    pstatement.setString(3, codigoArticulo);
                }else{
                    if(mes != -1 && codigoArticulo == "N/A"){
                        fechaIniBusq = new Date(year - 1900, mes, 1);
                        
                        if(mes == 11){
                            fechaFinBusq = new Date(year - 1900, mes, 31);
                            query = "select * from gastosindirectos where fecha >= ? and fecha <= ? order by(fecha) asc";
                        }else{
                            fechaFinBusq = new Date(year - 1900, mes + 1, 1);
                            query = "select * from gastosindirectos where fecha >= ? and fecha < ? order by(fecha) asc";
                        }
                        
                        pstatement = this.conn.prepareStatement(query);
                        pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));                            
                        pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));                            
                    }else{
                        if(mes == -1 && codigoArticulo == "N/A"){
                            fechaIniBusq = new Date(year - 1900, 0, 1);
                            fechaFinBusq = new Date(year - 1900, 11, 31);
                            query = "select * from gastosindirectos where fecha >= ? and fecha <= ? order by(fecha) asc";

                            pstatement = this.conn.prepareStatement(query);
                            pstatement.setDate(1, java.sql.Date.valueOf(dateformat.format(fechaIniBusq)));                        
                            pstatement.setDate(2, java.sql.Date.valueOf(dateformat.format(fechaFinBusq)));  
                            //pstatement.setString(3, codigoArticulo);
                        }                        
                    }
                }
            }
            
            resultado = pstatement.executeQuery();
            
            while(resultado.next()){
                GastoIndirecto gasto = new GastoIndirecto();
                gasto.setIdGIF(resultado.getInt("idgif"));
                gasto.setConcepto(resultado.getString("concepto"));
                gasto.setFecha(resultado.getDate("fecha"));
                gasto.setDescGastoif(resultado.getString("descgastoif"));
                gasto.setValor(resultado.getFloat("valor")); 
                
                gastos.add(gasto);
            }
                    
            pstatement.close();
            resultado.close();
        }catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Error al recuperar los registros de gastos de la base de datos.", "Error", javax.swing.JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        
        CerrarBD();
        System.out.println(dateformat.format(fechaIniBusq));
        System.out.println(dateformat.format(fechaFinBusq));
        
        return gastos;
    }
 
}
